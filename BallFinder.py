import cv2
import numpy as np

#method that literally does nothing because it is not useful in this situation
def nothing(x):
	pass

#create blank window to put trackbars on
cv2.namedWindow("Tracking")

#lower value trackbars
cv2.createTrackbar("LowerHue"       , "Tracking", 29, 255, nothing)
cv2.createTrackbar("LowerSaturation", "Tracking", 103, 255, nothing)
cv2.createTrackbar("LowerValue"     , "Tracking", 90, 255, nothing)
#upper value trackbars
cv2.createTrackbar("UpperHue"       , "Tracking", 80, 255, nothing)
cv2.createTrackbar("UpperSaturation", "Tracking", 255, 255, nothing)
cv2.createTrackbar("UpperValue"     , "Tracking", 255, 255, nothing)


#test image for static image testing
#frame = cv2.imread("smarties.png")


#set up video camera to port 0 of computer (usually default port)
cam = cv2.VideoCapture(0)

while True:
	#get frame from camera
	ret,frame = cam.read()

	#convert the color type of image from RGB to HSV-(Hue, Saturation, Value)
	hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

	#Collect values from trackbars in the "Tracking" window
	#lower bound trackbars
	l_h = cv2.getTrackbarPos("LowerHue"       , "Tracking")
	l_s = cv2.getTrackbarPos("LowerSaturation", "Tracking")
	l_v = cv2.getTrackbarPos("LowerValue"     , "Tracking")
	#upper bound trackbars
	u_h = cv2.getTrackbarPos("UpperHue"       , "Tracking")
	u_s = cv2.getTrackbarPos("UpperSaturation", "Tracking")
	u_v = cv2.getTrackbarPos("UpperValue"     , "Tracking")


	#lower bounds of mask
	l_b = np.array([l_h, l_s, l_v])
	#upper bounds of mask
	u_b = np.array([u_h, u_s, u_v])

	#IMAGE FILTERS
	#apply mask to hsv
	mask = cv2.inRange(hsv, l_b, u_b)
	#bitAnd = cv2.bitwise_and(frame, frame, mask=mask)
	#gray = cv2.cvtColor(bitAnd, cv2.COLOR_RGB2GRAY)
	blur = cv2.GaussianBlur(mask, (11,11),0)
	canny = cv2.Canny(blur, 50, 200)


	circles = cv2.HoughCircles(canny, cv2.HOUGH_GRADIENT, 3, 300)
 	
 	
	# ensure at least some circles were found
	if circles is not None:
		# convert the (x, y) coordinates and radius of the circles to integers
		circles = np.round(circles[0, :]).astype("int")
		# loop over the (x, y) coordinates and radius of the circles
		for (x, y, r) in circles:
			# draw the circle in the output image, then draw a rectangle
			# corresponding to the center of the circle
			cv2.circle(frame, (x, y), r, (0, 255, 0), 4)
			cv2.rectangle(frame, (x - 5, y - 5), (x + 5, y + 5), (0, 128, 255), -1)

	 

	#display specific windows
	cv2.imshow("image", frame)
	#cv2.imshow("mask", mask)
	#cv2.imshow("bitAND", bitAnd)
	#cv2.imshow("gray", gray)
	#cv2.imshow("blur", blur)
	#cv2.imshow("canny", canny)



	#check if the "esc" key is pressed. If so, all windows will close 
	key = cv2.waitKey(1) & 0xFF
	if key == 27:
		print(l_b)
		print(u_b)
		break

cv2.destroyAllWindows()